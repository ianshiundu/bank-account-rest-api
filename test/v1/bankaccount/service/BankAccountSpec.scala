package v1.bankaccount.service

import org.scalatest.PrivateMethodTester
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import v1.bankaccount.controllers.AccountFormInput
import v1.bankaccount.services.{Amount, BankAccount}

import scala.concurrent.ExecutionContext.Implicits.global

class BankAccountSpec extends PlaySpec with PrivateMethodTester with MockitoSugar with ScalaFutures {

  "Bank Account" must {

    "update balance when user deposit amount " in {
      val bankAccount = new BankAccount()
      bankAccount.deposit(AccountFormInput(3000))

      bankAccount.checkBalance().map { transaction ⇒
        transaction.balance.map{ accountBalance ⇒
          assert(accountBalance == 3000.00)
        }
      }
    }

    "update balance when user withdraws amount" in {
      val bankAccount = new BankAccount()

      bankAccount.deposit(AccountFormInput(3000))

      bankAccount.withdraw(AccountFormInput(1000))

      bankAccount.checkBalance().map { transaction ⇒
        transaction.balance.map{ accountBalance ⇒
          assert(accountBalance == 2000.00)
        }
      }
    }

    "throw an exception when depositing <= 0" in {
      val bankAccount = new BankAccount()

      whenReady(bankAccount.deposit(AccountFormInput(-0.1)).failed) { res ⇒
            assert(res.getLocalizedMessage == "amount must be greater than zero")

      }
    }

    "throw an exception when withdrawing <= 0" in {
      val bankAccount = new BankAccount()

      whenReady(bankAccount.withdraw(AccountFormInput(-0.1)).failed) { res ⇒
        assert(res.getLocalizedMessage == "amount must be greater than zero")

      }
    }

    "throw an exception when trying to exceed deposit limit per transaction" in {
      val bankAccount = new BankAccount()
      val maxDepositPerTransaction: Amount = 40000

      whenReady(bankAccount.deposit(AccountFormInput(50000)).failed) { res ⇒
        assert(res.getLocalizedMessage == s"You can only deposit $maxDepositPerTransaction per transaction!")
      }
    }

    "throw an exception when trying to exceed withdraw limit per transaction" in {
      val bankAccount = new BankAccount()
      val maxWithdrawalPerTransaction: Amount = 20000

      whenReady(bankAccount.withdraw(AccountFormInput(30000)).failed) { res ⇒
        assert(res.getLocalizedMessage == s"You can only withdraw $maxWithdrawalPerTransaction per transaction!")
      }
    }

    "throw an exception when trying to withdraw amount greater than balance" in {
      val bankAccount = new BankAccount()

      bankAccount.deposit(AccountFormInput(3000))

      whenReady(bankAccount.withdraw(AccountFormInput(5000)).failed) { res ⇒
        assert(res.getLocalizedMessage == "insufficient funds")
      }
    }

    "throw an exception when trying to exceed deposit frequency" in {
      val bankAccount = new BankAccount()

      bankAccount.deposit(AccountFormInput(10000.00))
      bankAccount.deposit(AccountFormInput(30000.00))
      bankAccount.deposit(AccountFormInput(20000.19))
      bankAccount.deposit(AccountFormInput(5000.99))
      bankAccount.deposit(AccountFormInput(100.20))
      bankAccount.deposit(AccountFormInput(2000.44))

      whenReady(bankAccount.deposit(AccountFormInput(1200.00)).failed) { res ⇒
        assert(res.getLocalizedMessage == s"You have exceeded the deposit limit of 4 per day. Try again later")
      }
    }

    "throw an exception when trying to exceed withdrawal frequency" in {
      val bankAccount = new BankAccount()
      bankAccount.deposit(AccountFormInput(10000.00))
      bankAccount.deposit(AccountFormInput(40000.00))
      bankAccount.deposit(AccountFormInput(40000.19))
      bankAccount.deposit(AccountFormInput(40000.00))

      bankAccount.withdraw(AccountFormInput(10000.00))
      bankAccount.withdraw(AccountFormInput(9000.00))
      bankAccount.withdraw(AccountFormInput(2000.19))
      bankAccount.withdraw(AccountFormInput(5000.19))

      whenReady(bankAccount.withdraw(AccountFormInput(20000.00)).failed) { res ⇒
        assert(res.getLocalizedMessage == s"You have exceeded the withdrawal limit of 3 per day. Try again later")
      }
    }

    "throw an exception when trying to exceed deposit limit per day" in {
      val bankAccount = new BankAccount()
      val maxDepositsPerDay: Amount = 150000

      bankAccount.deposit(AccountFormInput(40000.20))
      bankAccount.deposit(AccountFormInput(40000.30))
      bankAccount.deposit(AccountFormInput(40000.10))
      bankAccount.deposit(AccountFormInput(40000.19))
      bankAccount.deposit(AccountFormInput(40000.30))

      whenReady(bankAccount.deposit(AccountFormInput(1200.00)).failed) { res ⇒
        assert(res.getLocalizedMessage == s"You are trying to exceeded the deposit limit of $maxDepositsPerDay per day.")
      }
    }

    "throw an exception when trying to exceed withdraw amount limit per day" in {
      val bankAccount = new BankAccount()
      val maxWithdrawalPerDay: Amount = 50000

      bankAccount.deposit(AccountFormInput(20000.20))
      bankAccount.deposit(AccountFormInput(30000.30))
      bankAccount.deposit(AccountFormInput(30000.10))
      bankAccount.deposit(AccountFormInput(40000.10))
      bankAccount.deposit(AccountFormInput(39000.10))

      bankAccount.withdraw(AccountFormInput(20000.00))
      bankAccount.withdraw(AccountFormInput(20000.00))

      whenReady(bankAccount.withdraw(AccountFormInput(19000.00)).failed) { res ⇒
        assert(res.getLocalizedMessage == s"You are trying to exceeded the withdraw limit of $maxWithdrawalPerDay per day.")
      }


    }

  }
}
