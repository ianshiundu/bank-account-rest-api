package v1.bankaccount.controllers

import javax.inject.Inject
import play.api.Logger
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._
import v1.bankaccount.mapper.BankAccountJsonSupport
import v1.bankaccount.services.Amount
import v1.bankaccount.utils.{AccountBaseController, AccountControllerComponents}

import scala.concurrent.{ExecutionContext, Future}

case class AccountFormInput(amount: Amount)

class BankAccountController @Inject()(cc: AccountControllerComponents)(implicit ec: ExecutionContext) extends
  AccountBaseController(cc) with BankAccountJsonSupport {
  private val logger = Logger(getClass)

  private val accountForm: Form[AccountFormInput] = {
    import play.api.data.Forms._
    import play.api.data.format.Formats._

    Form(
      mapping(
        "amount" -> of[Amount]
      )(AccountFormInput.apply)(AccountFormInput.unapply)
    )
  }

  def deposit: Action[AnyContent] = AccountAction.async { implicit request ⇒
    accountForm.bindFromRequest.fold(
      error ⇒ {
        Future.successful(BadRequest(error.errorsAsJson))
      },
      depositInput ⇒ {
        bankAccountResource.deposit(depositInput).map { transaction ⇒
          logger.trace("deposit: ")
          Ok(Json.toJson(transaction))
        }
      }
    )
  }

  def withdraw: Action[AnyContent] = AccountAction.async { implicit request ⇒
    accountForm.bindFromRequest.fold(
      error ⇒ {
        Future.successful(BadRequest(error.errorsAsJson))
      },
      withdrawInput ⇒ {
        bankAccountResource.withdraw(withdrawInput).map { transaction ⇒
          logger.trace("withdraw amount: ")
          Ok(Json.toJson(transaction))
        }
      }
    )
  }

  def checkBalance: Action[AnyContent] = AccountAction.async { implicit request ⇒
    bankAccountResource.checkBalance().map { balance ⇒
      logger.trace("balance: ")
      Ok(Json.toJson(balance))
    }
  }
}
