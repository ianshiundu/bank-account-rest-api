package v1.bankaccount.mapper

import de.heikoseeberger.akkahttpplayjson._
import org.joda.time.DateTime
import play.api.libs.json._
import play.api.libs.json.jackson.PlayJsonModule
import v1.bankaccount.controllers.AccountFormInput
import v1.bankaccount.services.Transaction

trait BankAccountJsonSupport extends PlayJsonSupport {
  //  Joda mapper
  import play.api.libs.json.{JodaReads, JodaWrites}
  implicit val dateTimeWriter: Writes[DateTime] = JodaWrites.jodaDateWrites("dd/MM/yyyy HH:mm:ss")
  implicit val dateTimeJsReader: Reads[DateTime] = JodaReads.jodaDateReads("yyyyMMddHHmmss")

//  Object Mapper
  import com.fasterxml.jackson.databind.ObjectMapper
  implicit val mapper: ObjectMapper = new ObjectMapper().registerModule(PlayJsonModule)
  implicit val jsValue: JsValue = mapper.readValue("""{"foo":"bar"}""", classOf[JsValue])

//  Service mapper
  implicit val accountFormInputFormat: OFormat[AccountFormInput] = Json.format[AccountFormInput]
  implicit val transactionFormat: OFormat[Transaction] = Json.format[Transaction]

}
