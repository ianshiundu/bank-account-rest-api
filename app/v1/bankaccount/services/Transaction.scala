package v1.bankaccount.services

import org.joda.time.DateTime

// Open/closed principle
case class Transaction(balance: Option[AccountBalance] = None, depositFrequency: Option[Int] = None, withdrawFrequency: Option[Int] = None,
                       depositTs: Option[DateTime] = None, withdrawTs: Option[DateTime] = None, depositsPerDay: Option[Amount] = None,
                       withdrawalsPerDay: Option[Amount] = None) {

  def withBalance(balance: AccountBalance): Transaction = this.copy(balance = Option(balance))

  def withDepositFrequency(depositFrequency: Int): Transaction = this.copy(depositFrequency = Option(depositFrequency))

  def withWithdrawFrequency(withdrawFrequency: Int): Transaction = this.copy(withdrawFrequency = Option(withdrawFrequency))

  def withDepositTimestamp(depositTs: DateTime): Transaction = this.copy(depositTs = Option(depositTs))

  def withWithdrawTimestamp(withdrawTs: DateTime): Transaction = this.copy(withdrawTs = Option(withdrawTs))

  def withDepositsPerDay(depositsPerDay: Amount): Transaction = this.copy(depositsPerDay = Option(depositsPerDay))

  def withWithdrawalsPerDay(withdrawalsPerDay: Amount): Transaction = this.copy(withdrawalsPerDay = Option(withdrawalsPerDay))
}

object Transaction {
  val aTransaction = Transaction()
}
