package v1.bankaccount.services

import javax.inject.Inject
import org.joda.time.DateTime
import v1.bankaccount.controllers.AccountFormInput
import v1.bankaccount.services.Transaction.aTransaction

import scala.concurrent.{ExecutionContext, Future}

class BankAccount @Inject()() (implicit ec: ExecutionContext) extends BankAccountTrait {
  private var balance: AccountBalance = 0
  private var depositFrequency = 0
  private var withdrawFrequency = 0
  private var depositsPerDay: Amount = 0
  private var withdrawalsPerDay: Amount = 0

  private val date = DateTime.now()
  private val maxDepositPerTransaction: Amount = 40000
  private val maxWithdrawalPerTransaction: Amount = 20000
  private val maxDepositsPerDay: Amount = 150000
  private val maxWithdrawalPerDay: Amount = 50000

  private def hasDayPassed(ts: DateTime): Boolean = {
    new DateTime(ts).isBefore(DateTime.now().minusDays(1))
  }

  /* The f"$bal%1.2f" syntax is used to round of the amount to 2 d.p  */

  def deposit(input: AccountFormInput): Future[Transaction] = {
    Future {
      depositsPerDay += input.amount

      if (input.amount <= 0) throw new IllegalArgumentException("amount must be greater than zero")
      if (input.amount > maxDepositPerTransaction) {
        throw new IllegalArgumentException(s"You can only deposit $maxDepositPerTransaction per transaction!")
      }
      if (depositFrequency >= 4 && !hasDayPassed(aTransaction.depositTs.getOrElse(date))) {
        throw new IllegalArgumentException(s"You have exceeded the deposit limit of 4 per day. Try again later")
      }
      if (depositsPerDay > maxDepositsPerDay && !hasDayPassed(aTransaction.depositTs.getOrElse(date))) {
        depositsPerDay -= input.amount
        throw new IllegalArgumentException(s"You are trying to exceeded the deposit limit of $maxDepositsPerDay per day.")
      }

        balance += input.amount

        aTransaction.withBalance(f"$balance%1.2f".toDouble).withDepositFrequency({depositFrequency += 1 ; depositFrequency})
          .withDepositTimestamp(DateTime.now()).withDepositsPerDay(f"$depositsPerDay%1.2f".toDouble)

    }
  }

  def withdraw(input: AccountFormInput): Future[Transaction] = {
    Future {
      withdrawalsPerDay += input.amount

      if (input.amount <= 0) throw new IllegalArgumentException("amount must be greater than zero")
      if (input.amount > maxWithdrawalPerTransaction) {
        throw new IllegalArgumentException(s"You can only withdraw $maxWithdrawalPerTransaction per transaction!")
      }
      if (input.amount > balance) throw new IllegalArgumentException("insufficient funds")
      if (withdrawFrequency >= 3 && !hasDayPassed(aTransaction.withdrawTs.getOrElse(date))) {
        throw new IllegalArgumentException(s"You have exceeded the withdrawal limit of 3 per day. Try again later")
      }
      if (withdrawalsPerDay > maxWithdrawalPerDay && !hasDayPassed(aTransaction.withdrawTs.getOrElse(date))) {
        withdrawalsPerDay -= input.amount
        throw new IllegalArgumentException(s"You are trying to exceeded the withdraw limit of $maxWithdrawalPerDay per day.") }

        balance -= input.amount

        aTransaction.withBalance(f"$balance%1.2f".toDouble).withWithdrawFrequency({withdrawFrequency += 1 ; withdrawFrequency})
          .withWithdrawTimestamp(DateTime.now()).withWithdrawalsPerDay(f"$withdrawalsPerDay%1.2f".toDouble)
    }
  }

  def checkBalance(): Future[Transaction] = {
    Future {
      aTransaction.withBalance(f"$balance%1.2f".toDouble)
    }
  }
}

trait BankAccountTrait {
  def deposit(input: AccountFormInput): Future[Transaction]
  def withdraw(input: AccountFormInput): Future[Transaction]
  def checkBalance(): Future[Transaction]
}
