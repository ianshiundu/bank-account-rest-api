package v1.bankaccount

package object services {
  type AccountBalance = Double
  type Amount = Double
}
