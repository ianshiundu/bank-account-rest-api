package v1.bankaccount.routers

import javax.inject.Inject
import play.api.routing.Router.Routes
import play.api.routing.SimpleRouter
import play.api.routing.sird._
import v1.bankaccount.controllers.BankAccountController

class BankAccountRouter @Inject()(controller: BankAccountController) extends SimpleRouter {
  val prefix = "/v1/account"

  override def routes: Routes = {
    case GET(p"/balance") ⇒
      controller.checkBalance

    case POST(p"/deposit") ⇒
      controller.deposit

    case POST(p"/withdraw") ⇒
      controller.withdraw
  }

}
