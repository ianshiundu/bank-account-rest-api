# Bank Account REST API

This is a bank account rest api service built with Scala's Play framework.

## Appendix

### Running

To clone this project on your machine run this on your terminal:

```bash
git clone https://ianshiundu@bitbucket.org/ianshiundu/bank-account-rest-api.git
```

You need to download and install sbt for this application to run.

Once you have sbt installed, running the following at the command prompt will start up Play in development mode:

```bash
sbt run
```

Play will start up on the HTTP port at <http://localhost:9000/>.   You don't need to deploy or reload anything -- changing any source code while the server is running will automatically recompile and hot-reload the application on the next HTTP request. 

### Usage

To make a deposit, you make a POST request with the `Content-Type` specified to `application/json`.

Eg:

```bash
curl -X POST \
  http://localhost:9000/v1/account/deposit \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
	"amount" : 2000
}'
```

you'll then get a response like so:

```routes
{"balance":2000,"depositFrequency":1,"depositTs":"05/10/2018 13:09:23","depositsPerDay":2000}%
```

Likewise, you can make a withdrawal by sending a POST request like so:

```bash
curl -X POST \
  http://localhost:9000/v1/account/withdraw \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
	"amount" : 100
}'
```

and get:

```bash
{"balance":1900,"withdrawFrequency":1,"withdrawTs":"05/10/2018 13:15:37","withdrawalsPerDay":100}%
```

to return your outstanding balance, you make a GET request like so:

```bash
curl -X GET \
  http://localhost:9000/v1/account/balance \
  -H 'Cache-Control: no-cache' 
```

and get:
```bash
{"balance":1900}%
```

### Testing

To run the tests use the following command:


```bash
sbt test
```
or alternatively you can use your preferred IDE to run the tests.

To generate coverage report run this command:
```bash
sbt clean coverage test
```
Coverage reports will be in `target/scoverage-report`. 

### Checklist
* [ ] Program should have 3 endpoints Balance, Deposit and Withdrawal.
* [ ] Balance endpoint should return outstanding balance.
* [ ] Handle errors
* [ ] Write Tests
* [ ] Code is readable and can be run :)
* [ ] Check in code on Bitbucket and write instructions on README

##### Deposit Endpoint
* [ ] Max deposit for the day = $150K
* [ ] Max deposit per transaction = $40K
* [ ] Max deposit frequency = 4 transactions/day

##### Withdrawal Endpoint
* [ ] Max withdrawal for the day = $50K
* [ ] Max withdrawal per transaction = $20K
* [ ] Max withdrawal frequency = 3 transactions/day
* [ ] Cannot withdraw when balance is less than withdrawal amount





![](https://media.giphy.com/media/h19XT0qdBgJj2/giphy.gif)



